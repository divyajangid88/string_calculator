package SimpleCalculator;
import java.util.List;
import java.util.ArrayList;
import static org.junit.Assume.assumeFalse;

public class StringCalculator {
	public int Add(String input) {
			
		List<String> negate = new ArrayList<String>();
		if (input == null || input.isEmpty()) {
			return 0;
			
		}
		char isDelimiter = getDelimiter(input.split("\n")[0]);
		String[] result = (isDelimiter == ',')? input.split(",|\n"): input.split("\n|" + isDelimiter);
		int total=0;
		int skipFirstLine =(isDelimiter == ',')? 0 :2;
		for (String item : result) {
			if(skipFirstLine > 0) {
				--skipFirstLine;
			}else {
				int num=Integer.parseInt(item);
				if(num<0) {
					negate.add(item);
				}
				total += num;
			}
		}
		if(!negate.isEmpty()) {
			throw new IllegalArgumentException("negative not allowed" + String.join(",", negate));
		}
		return total;
				}
	private char getDelimiter(String line) {
		if(line == null || line.isEmpty()) {
			return ',';
			}
		if(isNumeric(line)) {
			return ',';
		}
		if (line.length()==1) {
			return line.charAt(0);
		}
		return ',';
	}
	private boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);}
		catch(NumberFormatException nfe) {
			return false;}
		return true;
		}
	
}
