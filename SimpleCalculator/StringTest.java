package SimpleCalculator;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringTest {

	@Test
	public void ForEmptyString() {
		StringCalculator sc = new StringCalculator();
		
		assertEquals(0, sc.Add(""));
	}
	
	@Test
	public void ForOneNumber() {
     StringCalculator sc = new StringCalculator();
		
		assertEquals(8, sc.Add("8"));
	}
	@Test
	public void ForTwoNumber() {
       StringCalculator sc = new StringCalculator();
		
		assertEquals(5, sc.Add("2,3"));
	}
	
	@Test
	public void ForLongDigit() {
     StringCalculator sc = new StringCalculator();
		
		assertEquals(75, sc.Add("50,25"));
	}
     @Test
     public void ForallDigit() {
    	 StringCalculator sc = new StringCalculator();
 		
 		assertEquals(15, sc.Add("1,2,3,4,5"));
     }
     @Test
     public void ForCustomDelimiter() {
    	 StringCalculator sc = new StringCalculator();
 		
 		assertEquals(15, sc.Add(";\n1;2;3;4;5"));
     }
    
}
