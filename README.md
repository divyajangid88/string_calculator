**String Calculator**

String Calculator is a implementation of  the calculator that take input in string and perform add operation on input data.


**Functions**

* Create a simple String calculator with a method int add(String input).
* The string argument can contain 0, 1 or 2 numbers, and will return their sum (for an empty string it will return 0) for example "" or "1" or "1,2".
* Allow the add method to handle newlines as separators: "1\n2,3" should return "6".
* Calling add with negative numbers will return the message "Negative not allowed




**Dependencies**
* Eclipse
* Junit4
* Java